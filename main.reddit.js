const Express = require('express')
const App = Express()
const Path = require('path')
const Fs = require('fs')
const expectedExtensions = [".jpg",".png"]
const ListPath = Fs.readdirSync('./images')
  .filter(path=>
    ~expectedExtensions.indexOf(Path.extname(path))
  )
App.use('/',(Express.static(__dirname+'/')))
App.get(/%%(.*)%%/,(req,res)=>
{
	console.log(req.url)
  const requestName = req.params[0]
  for(let path of ListPath)
  {
    const name = path.slice(0,-4)
    if(name == requestName)
    {
      res.sendFile(__dirname+'/images/'+path)
      break;
    }
  }
})
App.listen(8080)
